let image = document.querySelectorAll('img');
let btnPause = document.querySelector('#Pause');
let btnStart = document.querySelector('#Start');

let time = 0;
slideshow()
let timeline = setInterval(slideshow, 3000);


btnStart.addEventListener('click', () => {
  clearInterval(timeline);
  timeline = setInterval(slideshow, 3000);
})
btnPause.addEventListener('click', () => {
  clearInterval(timeline);
})

function slideshow() {
  if (time < 3) {
    time++;
  } else {
    time = 0;
  }

  image.forEach((element, index) => {
      element.style.display = 'none';
      image[time].style.display = 'block';
  })
}
