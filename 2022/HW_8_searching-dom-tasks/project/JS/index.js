// 1)
let p = document.querySelectorAll('p');
for (let elem of p) {
    elem.style.background = '#ff0000';
}

console.log('2)'); // 2)
let elemID = document.getElementById("optionsList");
console.log(elemID);
console.log(elemID.childNodes);

// 3)
let newElem = document.createElement("p");
newElem.innerText = "This is a paragraph";
newElem.classList.add("testParagraph");
document.body.appendChild(newElem);

console.log('4)'); // 4)
let elemHeader = document.querySelector('.main-header');
let elemLI = elemHeader.querySelectorAll("li");
console.log(elemLI);
for (let i of elemLI){
    i.classList.add("nav-item");
}

// 5)
let elemRemove = document.querySelectorAll(".section-title");
for (let i of elemRemove){
    i.classList.remove("section-title")
}