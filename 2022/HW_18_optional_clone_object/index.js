let arr_1 = [5,[2,'4'],7,9];
let arr_2 = [];

function copyArr(arr_2, arr_1, index = 0) {
  if (index == arr_1.length) {
    return arr_2
  }
  arr_2.push(arr_1[index]);
  copyArr(arr_2, arr_1, ++index);
}
copyArr(arr_2, arr_1)

console.log(arr_1,' => ', arr_2);