let body = document.body;
let link = document.querySelector('link');
body.style.paddingTop = '50px'

let bool = true;
body.addEventListener('click', (elem)=>{
  if (elem.target.tagName != 'TD') {
    if (bool){
      bool = false;
      link.setAttribute('href', './CSS/akt.css')
    }
    else {
      bool = true;
      link.setAttribute('href', './CSS/pas.css')
    }
  }
})

let table = document.createElement('table');
body.appendChild(table);
table = document.querySelector('table');

for(let i = 0; i < 30; i++){
  let tr = document.createElement('tr');
  table.appendChild(tr);

  for(let i = 0; i < 30; i++){
    let td = document.createElement('td');
    td.style.height = '46px';
    td.style.width = '46px';
    td.setAttribute('class', 'white')
    tr.appendChild(td)
  }
}
let trArr = document.querySelectorAll('td');
trArr.forEach((elem)=>{
  elem.addEventListener('click', ()=>{
    elem.className == 'white' ? elem.setAttribute('class', 'black') : elem.setAttribute('class', 'white');
  })
})