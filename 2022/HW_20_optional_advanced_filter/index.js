exampleObj = [
  {name: '00', description:'0000', contentType: {name: 'mshgshs'}, locales: {description:'en_US', name: 'GREHERH'}},
  {name: 'SBDSBD1', description:'en_US', contentType: {name: 'THNVCXZzVBC'},locales: {description:'Toyota', name: 'ASBDXHDGF'}},
  {name: 'BVMJTYURYTWEFZ', description:'NFRTRGSDV', contentType: {name: 'CVXBHDRSFAS'}, locales: {description:'TDGRX', name: 'HTGSDV'}},
]

function filterCollection(vehicles = [], filterWord, bool){
  filterWordArr = filterWord.split(' ');
  let resultArr = [];
  let argumentsArr = [...arguments].slice(3);
  argumentsArr = argumentsArr.map(value => value.split('.').length - 1 ? value.split('.') : value);
  vehicles.forEach(value => {
    let indexSuccess = 0;
    argumentsArr.forEach(key => {
      filterWordArr.forEach(word => {
        if (typeof key === 'object') {
          indexSuccess += value[key[0]][key[1]] == word;
        }
        else {
          indexSuccess += value[key] == word;
        }
      })
      
    })
    if (bool) {
      if (indexSuccess == filterWordArr.length) {
        resultArr.push(value);
      }
    }
    else {
      if (indexSuccess >= 1) {
        resultArr.push(value);
      }
    }
  })
  
  return resultArr
}

console.log(filterCollection(exampleObj, 'en_US Toyota', false, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));