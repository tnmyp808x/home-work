let btnChangeThem = document.querySelector('.changeThem');
let linkCss = document.querySelectorAll('link');


if (localStorage.getItem('darkThem') == "false") {
  linkCss[1].setAttribute('href', './css/style.css');
}
else {
  linkCss[1].setAttribute('href', './css/dark.css')
}

btnChangeThem.addEventListener('click' , () => {
  if (localStorage.getItem('darkThem') == "true") {
    linkCss[1].setAttribute('href', './css/style.css');
    localStorage.setItem('darkThem', false)
  }
  else {
    linkCss[1].setAttribute('href', './css/dark.css')
    localStorage.setItem('darkThem', true)
  }
})