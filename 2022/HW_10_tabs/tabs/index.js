let elemUL = document.querySelector('.tabs');
let elemLI = elemUL.querySelectorAll('li');
let elemContent = document.querySelector('.tabs-content').querySelectorAll('li');

for (let i of elemContent){
    i.style.display = "none";
}
document.querySelector('.tabs-content').querySelector('li').style.display = "block";

document.querySelector('.tabs').addEventListener('click', event => {
    // console.log(event.target.innerText);
    let elemName = event.target.innerText;

    elemContent.forEach(elem => {
        elem.style.display = "none";
        for (let i of elemContent){
            if (i.classList == elemName){
                i.style.display = "block";
            }
        }
    });
});

