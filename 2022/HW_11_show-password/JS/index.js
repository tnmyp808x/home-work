let PSW = document.querySelectorAll('label');
let inputPSW = document.querySelectorAll('input');
let icon = document.querySelectorAll('i');
let btnPSW = document.querySelector('button');
let errorText = document.querySelector('samp');

icon.forEach((element, index) => {
  element.addEventListener('click', () => {
    if (element.classList.value == 'fas fa-eye icon-password') {
      inputPSW[index].setAttribute('type', 'text');
      element.setAttribute('class', 'fas fa-eye-slash icon-password');
    }
    else {
      inputPSW[index].setAttribute('type', 'password');
      element.setAttribute('class', 'fas fa-eye icon-password');
    }
  })
})

console.log(btnPSW);
btnPSW.addEventListener('click', () => {
    if (inputPSW[0].value == inputPSW[1].value){
      errorText.innerText = '';
      alert('You are welcome');
    }
    else {
        errorText.innerText = 'Нужно ввести одинаковые значения';
    }
})