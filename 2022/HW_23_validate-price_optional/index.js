let body = document.body;
let input = document.querySelector('.ipt');
let text  = document.querySelector('.text');
let btn = document.querySelector('.btn');

body.addEventListener('click', (elem) => {
  if (elem.target.tagName != 'INPUT') {
    if (input.classList[1]) {
      if (input.value < 0) {
        input.style.border = '3px solid #ff0000';
        text.innerHTML = `Please enter correct price`;
      }
      else {
        text.innerHTML = `Текущая цена: ${input.value}`;
      }
      input.classList.toggle('cheak');
      input.style.color = '#00ff00';
      btn.style.display = 'block';
    }
  }
})

input.addEventListener('click', ()=>{
  input.classList.toggle('cheak');
})
btn.addEventListener('click', (event)=>{
  event.preventDefault();
  input.value = '';
  text.innerHTML = '';
  btn.style.display = 'none';
  input.style.border = '3px solid #9c9c9c';
  input.style.color = 'black';
})