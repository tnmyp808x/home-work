function createNewUser(firstName, lastName, birthday) {
    const newUser = {
        firstName: firstName,
        lastName: lastName,
        getLogin(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge(){
            const date = new Date;
            if (date.getMonth() + 1 >= birthday.slice(3, 5)) {
              if (date.getDay() + 7 >= birthday.slice(0, 2)) {
                return date.getFullYear() - birthday.slice(6);
                
              }
            }
            return date.getFullYear() - birthday.slice(6) - 1;
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthday.slice(6);
        }
    }
    return(newUser);
}
console.log(createNewUser("Names", "LastName", "05.05.1995"));
console.log(createNewUser("Names", "LastName").getLogin());
console.log(createNewUser("Names", "LastName", "05.05.1995").getAge());
console.log(createNewUser("Names", "LastName", "05.05.1995").getPassword());