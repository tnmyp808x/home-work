let arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let body = document.body;
function list(arr, parent){
    let elemUL = document.createElement('ul');
    for (let i of arr.flat()){
        let elemLI = document.createElement('li')
        elemLI.innerText = i;
        elemUL.appendChild(elemLI);
    }
    parent.appendChild(elemUL);
}
list(arr, body);

function del() {
    let a = document.querySelector('ul');
    document.body.removeChild(a);
}
setTimeout(del, 3000)