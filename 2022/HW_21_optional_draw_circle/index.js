let btn = document.querySelector('#btn');
btn.style.width = '150px';
btn.style.height = '30px';
let box = document.createElement('div');
document.body.appendChild(box);
box.style.display = 'flex';
box.style.flexWrap = 'wrap';


btn.addEventListener('click', ()=>{
  let x = prompt('диаметр круга');
  if (x <= 0) {
    return false
  }
  else {
    let clc = confirm('Нарисовать');
    if (clc == true) {
      for (let i = 0; i < 100; i++) {
        CreateCircle(x);
      }
    }
    let ter = box.querySelectorAll('div');
    ter.forEach((elem)=>{
      elem.addEventListener('click', ()=> elem.remove())
    })
  }
})
function RandomColor() {
  let letters = '0123456789ABCDEF';
  let color = '#';
  for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
function CreateCircle(x) {
  let elem = document.createElement('div');
  elem.style.width = `${x}px`;
  elem.style.height = `${x}px`;
  elem.style.margin = '1vh';
  elem.style.borderRadius = `${x/2}px`;
  elem.style.background = `${RandomColor()}`;
  box.appendChild(elem);
}
