//Цикл нужен для проверок пароль и то что надо повторять.

"use strict"
do {
  let num = prompt("Enter multiples of 5");
  if (!num) {
    alert("Sorry, no numbers");
  } if (num >= 0 || Number.isInteger(num)) {
      const num5 = num / 5;
      console.log(0);
      for (let z = +1; z <= num5; z++) {
        console.log(z * 5);
      }
      break;
  } else {
    alert("Number not multiples of 5");
  }
} while(true);