import gulp from "gulp";
import { deleteAsync } from "del";
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import cleanCss from "gulp-clean-css";
import imagemin from 'gulp-imagemin';
import newer from 'gulp-newer';
import browsersync from "browser-sync";
import uglify from "gulp-uglify";
import rename from "gulp-rename";

const buildFolder = "./dist/";
const srcFolder = "./src/";

const path = {
  build: {
    css: `${buildFolder}/css/`,
    js: `${buildFolder}/js/`,
    html: `${buildFolder}/`,
    files: `${buildFolder}/`,
    img: `${buildFolder}/img/`
  },
  src: {
    scss: `${srcFolder}/scss/*.scss`,
    js: `${srcFolder}/js/*.js`,
    html: `${srcFolder}/*.html`,
    files: `${srcFolder}/**/*.*`
  },
  clean: `${buildFolder}/**`,
  images: `${srcFolder}/img/*.*`
}

const cssStyle = () => {
  return gulp.src(path.src.scss, {sourcemaps: true})
    .pipe( sass({ outputStyle: 'expanded'}))
    .pipe( cleanCss())
    .pipe( rename('styles.min.css'))
    .pipe( gulp.dest( path.build.css))
    .pipe(browsersync.stream());
}

const imageOpt = () => {
  return gulp.src( path.images )
    .pipe(newer( path.build.img ))
    .pipe(imagemin({
      verbose: true
    }))
		.pipe(gulp.dest( path.build.img ))
}

const html = () => {
  return gulp.src( path.src.html )
    .pipe( gulp.dest(path.build.html) )
}

const reset = () => {
  return deleteAsync(path.clean);
}

const server  = (done) => {
  browsersync.init({
    server: {
      baseDir: `${path.build.html}`
    },
    notify: false,
    port: 3000
  });
}

const jsOpt = () => {
  return gulp.src( path.src.js)
    .pipe( uglify())
    .pipe( rename('scripts.min.js'))
    .pipe( gulp.dest( path.build.js))
}

const mainTasks = gulp.series(reset, cssStyle, imageOpt, jsOpt, html)
const dev = gulp.series(mainTasks, server);// reset, , imageOpt
const build = gulp.series(mainTasks)
const scss = gulp.series(html, cssStyle, server)

gulp.task( "dev", dev );
gulp.task( "build", build);
gulp.task( "scss", scss);