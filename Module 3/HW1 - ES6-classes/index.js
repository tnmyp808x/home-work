class Employee {
  constructor (name, age, salary){
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  
}

class Programmer extends Employee {
  constructor (name, age, salary, lang){
    super(name, age, salary);
    this.lang = lang;
  }
  get salarySum() {
    this.salary = salary*3;
  }
}
let programmer1 = new Programmer ('name1', 20, 550, 'ua');
console.log(programmer1);

let programmer2 = new Programmer ('name2', 19, 1000, 'en');
console.log(programmer2);

let programmer3 = new Programmer ('name3', 25, 750, 'ru');
console.log(programmer3);
