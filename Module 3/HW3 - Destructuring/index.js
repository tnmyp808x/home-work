console.log('____1____');
const clients1 = ["Гилберт", "Сальваторе",1, "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман",1, "Сальваторе", "Майклсон"];

const newArray = [...new Set([].concat(clients1, clients2))];
console.log(newArray);

console.log('____2____');
const characters = [
  {
    name: "Елена",
    lastName: "Гилберт",
    age: 17, 
    gender: "woman",
    status: "human"
  },
  {
    name: "Кэролайн",
    lastName: "Форбс",
    age: 17,
    gender: "woman",
    status: "human"
  },
  {
    name: "Аларик",
    lastName: "Зальцман",
    age: 31,
    gender: "man",
    status: "human"
  },
  {
    name: "Дэймон",
    lastName: "Сальваторе",
    age: 156,
    gender: "man",
    status: "vampire"
  },
  {
    name: "Ребекка",
    lastName: "Майклсон",
    age: 1089,
    gender: "woman",
    status: "vempire"
  },
  {
    name: "Клаус",
    lastName: "Майклсон",
    age: 1093,
    gender: "man",
    status: "vampire"
  }
];

let charactersShortInfo = [];
characters.forEach( elem => {
  charactersShortInfo.push({
    name: elem.name,
    lastName: elem.lastName,
    age: elem.age
  })
})
console.log(charactersShortInfo);

console.log('____3____');
const user1 = {
  name: "John",
  years: 30
};

let [name, years] = [user1.name, user1.years];

if (user1.isAdmin == undefined) {
  user1.isAdmin  = false;
}
console.log(user1);

console.log('____4____');
const satoshi2020 = {
  name: 'Nick',
  surname: 'Sabo',
  age: 51,
  country: 'Japan',
  birth: '1979-08-21',
  location: {
    lat: 38.869422, 
    lng: 139.876632
  }
}

const satoshi2019 = {
  name: 'Dorian',
  surname: 'Nakamoto',
  age: 44,
  hidden: true,
  country: 'USA',
  wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
  browser: 'Chrome'
}

const satoshi2018 = {
  name: 'Satoshi',
  surname: 'Nakamoto', 
  technology: 'Bitcoin',
  country: 'Japan',
  browser: 'Tor',
  birth: '1975-04-05'
}

let fullProfile = Object.assign(satoshi2018, satoshi2019, satoshi2020);
console.log(fullProfile);

console.log('____5____');
const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

let newBooks = books;
newBooks.push(bookToAdd);

console.log(newBooks);

console.log('____6____');
const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}
let newEmployee = employee;
console.log(newEmployee);

console.log('____7____');
const array = ['value', () => 'showValue'];

// Допишіть код тут

alert(array[0]); // має бути виведено 'value'
alert(array[1]);  // має бути виведено 'showValue'